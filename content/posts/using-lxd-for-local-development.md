# Using LXD for local development

0. Create a personal SSH key

0. Create a profile

```
lxc profile list  # shows all available profiles
lxc profile show <profile-name>  # shows profile details
```

1. Create an lxc container with the `default` and `ricardo` profiles

   The `ricardo` lxc profile will setup:

   - Correct mappings for being able to access `~/dev` from the host
   - Passworldess sudo
   - Import the SSH key associated with `ricardogsilva` from github

   For container names, use the following format:

       <project_code>-dev


   ```bash
   container_base=ubuntu:16.04
   container_name=test-dev

   lxc launch \
       ${container_base} \
       ${container_name} \
       -p default \
       -p ricardo
   ```

2. Get the IP of the newly created container with `lxc list`

3. Setup an SSH config for easy login. Edit `~/.ssh/config` with something 
   like:

   ```
   cat << EOF >> ~/.ssh/config

   Host <project_code-dev>
       HostName <container IP>
       User ricardo
       IdentityFile ~/.ssh/ricardo_silva

   EOF
   ```


4. SSH into the container and do some updates. If needed, add the ubuntugis ppa
   too:

   ```
   ssh <project_code-dev>
   sudo add-apt-repository ppa:ubuntugis/ubuntugis-unstable
   sudo apt update && sudo apt upgrade --yes

4. Update locales

5. Add `~/.profile` and `~/.bashrc` files

6. Now install project specific stuff, e.g.:

   ```
   sudo apt install --yes \
       openjdk-8-jre
   ```

7. If using postgresql, tweak the relevant settings files to allow external
   access

   ```
   # sudo vim /etc/postgresql/9.5/main/postgresql.conf
   listen_addresses = '*'

   # sudo vim /etc/postgresql/9.5/main/pg_hba.conf
   host  all  all  10.0.1.0/24  md5

   # reload postgres server
   sudo systemctl reload postgresql
   ```


   ```psql
   # create a user and database
   CREATE USER <project_code> WITH PASSWORD '<project_code>';
   CREATE DATABASE <project_code> WITH OWNER '<project_code>';
   \c <project_code>
   CREATE EXTENSION postgis;
   ```





## Useful lxc commands

```
lxc list  # show current containers, along with their IP
lxc profile list  # list available profiles
lxc profile show <profile>  # show profile details
```

## Reference

-  Contents of the `ricardo` lxc profile

```

```
