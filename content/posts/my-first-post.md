---
title: My First Post
date: 2017-08-23 14:05:20 +0100
draft: true

---
This is my new blog. I'm currently in the process of setting it up, so there
isn't much to see here for now. Do come back a bit later though.

Thanks

(In the meanwhile, contemplate this nice bear image)

![bear](https://upload.wikimedia.org/wikipedia/commons/b/bf/Japanese_black_bear_1.jpg)

Image attribution: [CC BY 2.0](http://creativecommons.org/licenses/by/2.0)

This is a test of forestry.io