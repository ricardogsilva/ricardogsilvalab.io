# Installing keycloak on an LXD container

This method is suitable for development, it is not production-ready.

- Create your development LXD container, as mentioned in another post

- Download keycloak and use `scp` to send the downloaded file over to the
  container

- SSH into the container

- Untar keycloak

- Edit the configuration files, changing the bind addresses to 0.0.0.0 and the
  ports configuration to be offset by 100. This means editing the
  `/standalone/configuration/standalone.xml` file. Look for the `interfaces`
  and `socket-binding-group` sections.

  Further info in:

  - https://www.keycloak.org/docs/4.0/server_installation/

- Add a keycloak admin user with the `bin/add-user-keycloak.sh` script

  ```
  ./bin/add-user-keycloak.sh -r master -u <username> -p <password>
  ```

  Further info in:

  - https://www.keycloak.org/docs/4.0/server_admin/#server-initialization


- Start the keycloak server

- Use you web browser to access keycloak's UI on

  http://<ip-address-of-lxd-container>:8180/auth/admin
